//
//  Extension.swift
//  FirebaseTutorial
//
//  Created by Quang Anh on 01/12/2020.
//

import Foundation
import UIKit

extension UIView{
    func setCircle(){
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
// ex UIViewcontroller
extension UIViewController{
    func showAlert(_ title:String , _ message:String , handleOke: ((UIAlertAction) -> (Void))? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okeAction = UIAlertAction(title: "Ok", style: .default, handler: handleOke)
        alert.addAction(okeAction)
        self.present(alert, animated: true, completion: nil)
    }
    func showLeftBar(){
        self.addLeftBarButtonWithImage(UIImage(named: "menu") ?? .add)
    }
    func showRighBarButton(actionButton:Selector?,title:String){
        let button = UIBarButtonItem(title: title, style: .plain, target: self, action: actionButton)
        navigationItem.rightBarButtonItem = button
        
    }
    func showAlertWithTextField(title:String?,message:String?,titleAction:String,textEdit:String = "",actionHandler: ((_ text: String?) -> Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Input here"
            textField.text = textEdit
        }
        alert.addAction(UIAlertAction(title: titleAction, style: .default, handler: { (_) in
            guard let text = alert.textFields![0].text else {return}
            actionHandler?(text)
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
}

extension Notification.Name{
    static var notificationLoginSuccess = Notification.Name("notificationLoginSuccess")
}
