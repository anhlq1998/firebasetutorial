//
//  Constant.swift
//  FirebaseTutorial
//
//  Created by Quang Anh on 01/12/2020.
//

import Foundation
import UIKit
import Firebase
enum TypeAuthentication:String {
    case login    =  "Login"
    case register =  "Register"
}

func isValidEmail(_ email: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: email)
}

enum ResultRequest<T>{
    case sucsses(_ result :AuthDataResult?)
    case error(_ err :Error?)
}

enum TypeSildeMenu : Int {
    case firebaseAuthen = 0
    case firebaseRealtime
    case firebaseFirstore 
    case firebaseFCM
    case firebaseCrash
}

