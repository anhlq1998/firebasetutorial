//
//  FirebaseManager.swift
//  FirebaseTutorial
//
//  Created by Quang Anh on 01/12/2020.
//

import Foundation
import UIKit
import FirebaseAuth
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseDatabase
import FirebaseFirestore

// MARK: AUTHENTICATION
class FirebaseManager{
    
    static func loginEmail(email:String,password:String,completion: @escaping ((ResultRequest<Any>)->(Void))){
        FirebaseManager.logout()
        Auth.auth().signIn(withEmail: email, password: password) { (result, err) in
            if err == nil {
                completion(.sucsses(result))
                return
            }
            completion(.error(err))
            
            
        }
        
    }
    
    static func createUserWithGoogle(view:UIViewController){
        FirebaseManager.logout()
        GIDSignIn.sharedInstance()?.presentingViewController = view
        GIDSignIn.sharedInstance()?.signIn()
        
    }
    
    static func createUserWithFacebook(view:UIViewController){
        FirebaseManager.logout()
        let fbLogin = LoginManager()
        fbLogin.logOut()
        fbLogin.logIn(permissions: ["public_profile", "email"], from: view) { (result, err) in
            if err == nil {
                if result?.isCancelled == true {
                    print("cancle fb")
                    return
                }
                let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                Auth.auth().signIn(with: credential) { (result, err) in
                    if err == nil {
                        NotificationCenter.default.post(name: .notificationLoginSuccess, object: nil)
                        return
                    }
                    print("Err \(err?.localizedDescription)")
                }
                return
            }
            print("Login faild \(err?.localizedDescription)")
        }
    }
    
    static func logout(){
        do {
            try Auth.auth().signOut()
        } catch let err {
            print("Logout faild \(err.localizedDescription)")
        }
    }
    
    static func registerUserWithEmail(email:String,password:String,completion: @escaping ((ResultRequest<Any>)->(Void))){
        FirebaseManager.logout()
        Auth.auth().createUser(withEmail: email, password: password) { (resutl, err) in
            if err == nil {
                completion(.sucsses(resutl))
                return
            }
            completion(.error(err))
        }
    }
    
}

//MARK: FirebaseRealtime
extension FirebaseManager {
    // data base
    static var database : DatabaseReference = Database.database().reference()
    static var db = Firestore.firestore()
}
