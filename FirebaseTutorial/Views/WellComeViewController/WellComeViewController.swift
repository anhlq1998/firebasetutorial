//
//  WellComeViewController.swift
//  FirebaseTutorial
//
//  Created by Quang Anh on 02/12/2020.
//

import UIKit

class WellComeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btnGotoFirebase(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
}
