//
//  ListMenuViewController.swift
//  FirebaseTutorial
//
//  Created by Quang Anh on 02/12/2020.
//

import UIKit

class ListMenuViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var arr : [String] = ["Authentication","FirebaseRealtime","FirebaseFireStore"]
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func createEvent() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        
    }

}

extension ListMenuViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = arr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = TypeSildeMenu(rawValue: indexPath.row){
            changeMainSlideViewController(menu)
        }
    }
    
}
extension ListMenuViewController{
    func changeMainSlideViewController(_ menu : TypeSildeMenu){
        self.slideMenuController()?.changeMainViewController(mainSlideViewController(menu), close: true)
        
    }
    func mainSlideViewController(_ menu : TypeSildeMenu) -> UINavigationController{
        switch menu {
        case .firebaseAuthen:
            let vc = LoginViewController()
            let navigation = UINavigationController(rootViewController: vc)
            return navigation
        case .firebaseRealtime:
            let vc = FirebaseRealTimeViewController()
            let navigation = UINavigationController(rootViewController: vc)
            return navigation
        case .firebaseFirstore:
            let vc = FirebaseFirestoreViewController()
            let navigation = UINavigationController(rootViewController: vc)
            return navigation
        case .firebaseFCM:
            let vc = LoginViewController()
            let navigation = UINavigationController(rootViewController: vc)
            return navigation
        case .firebaseCrash:
            let vc = LoginViewController()
            let navigation = UINavigationController(rootViewController: vc)
            return navigation
        }
    }
}
