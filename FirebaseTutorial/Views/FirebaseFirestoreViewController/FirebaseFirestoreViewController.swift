//
//  FirebaseFirestoreViewController.swift
//  FirebaseTutorial
//
//  Created by Le Quang Anh on 12/7/20.
//

import UIKit

class FirebaseFirestoreViewController: BaseViewController {
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showLeftBar()
        self.title = "Firebase Firestore"
        // Do any additional setup after loading the view.
    }
    override func createEvent(){
        FirebaseManager.db.collection("user").addDocument(data: [
            "first":"Ada",
            "last":"Lovelace",
            "born":"1815",
        ]) { (err) in
            if let err = err {
                print("err \(err)")
            }else{
                print("Document add with id ")
            }
        }
    }
    

}
