//
//  FirebaseRealTimeViewController.swift
//  FirebaseTutorial
//
//  Created by Quang Anh on 02/12/2020.
//

import UIKit
import FirebaseDatabase
class FirebaseRealTimeViewController: BaseViewController {
    
    var arr : [ModelFirebaseRealtime] = []
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showRighBarButton(actionButton: #selector(showMessage), title: "add")
        
    }
        
    override func createEvent() {
        // event here
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.observeFirebaseRealtime()
        self.showLeftBar()
        
    }
    override func fetchData() {
        // data here
//        self.getDatabase()
    }
    @objc func showMessage(){
//        self.showAlert("THong Bao", "thong baos")
        self.getDatabase()
    }


}

extension FirebaseRealTimeViewController {
    func getDatabase(){
        self.showAlertWithTextField(title: "Thông Báo", message: "Thêm thông tin", titleAction: "Thêm") { (text) in
            guard let value = text else {return}
            print("value \(value)")
            if value.isEmpty {
                self.showAlert("Thông báo ", "Bạn không được để trống thông tin")
                return
            }
            FirebaseManager.database.child("user").childByAutoId().setValue(["name":"\(value)"])
        }
        
    }
    func observeFirebaseRealtime(){
        // real time
        FirebaseManager.database.child("user").observe(.value) { (database) in
            
            var arModel : [ModelFirebaseRealtime] = []
            
            for i in database.children{
                let arrModel : ModelFirebaseRealtime = ModelFirebaseRealtime()
                let snap = i as! DataSnapshot
                arrModel.id = snap.key.description
                guard let children = snap.value as? [String:Any] else {return}
                arrModel.title = children["name"] as! String
                arModel.append(arrModel)
                
            }
            self.arr = arModel
            self.tableView.reloadData()
           
        }
    }
    
    
    
    
}

extension FirebaseRealTimeViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = arr[indexPath.row].title
        return cell
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let id = arr[indexPath.row].id else {return}
            print("ID \(id) --")
            FirebaseManager.database.child("user").child("\(id)").removeValue()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = arr[indexPath.row].id else {return}
        self.showAlertWithTextField(title: "Thông Báo", message: "Thay đổi thông tin", titleAction: "Thay đổi",textEdit: "\(arr[indexPath.row].title!)") { (text) in
            guard let value = text else {return}
            print("value \(value)")
            if value.isEmpty{
                self.showAlert("Thông báo", "bạn không được để trống thông tin")
                return
            }
            FirebaseManager.database.child("user").child("\(id)").setValue(["name":"\(value)"])
        }
    }
    
}








class ModelFirebaseRealtime {
    var id : String?
    var title:String?
    init() {
    }
}
