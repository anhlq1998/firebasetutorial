//
//  LoginViewController.swift
//  FirebaseTutorial
//
//  Created by Quang Anh on 01/12/2020.
//

import UIKit

class LoginViewController: BaseViewController{

    @IBOutlet weak var btnSwitch: UISwitch!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var stViewLoginApp: UIStackView!
    
    var type : TypeAuthentication = .login

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showLeftBar()
    }
    override func createEvent() {
        self.handleNotificationCenter()
    }
    
    @IBAction func btnLoginGG(_ sender: Any) {
        self.loginGG()
    }
    
    @IBAction func btnLoginFB(_ sender: Any) {
        self.loginFB()
    }
    @IBAction func btnSwitch(_ sender: Any) {
        if btnSwitch.isOn {
            type = .register
        }else {
            type = .login
        }
        DispatchQueue.main.async {
            self.stViewLoginApp.isHidden = self.btnSwitch.isOn ? true : false
            self.lbTitle.text = self.type.rawValue
            self.btnLogin.setTitle(self.type.rawValue.uppercased(), for: .normal)
            self.tfEmail.text = ""
            self.tfPassword.text = ""
        }
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        guard let email = tfEmail.text else{return}
        guard let password = tfPassword.text else {return}
        self.loginEmail(email, password)
    }
}
extension LoginViewController {
    // login email
    func loginEmail(_ email:String , _ password:String){
        if email.isEmpty && password.isEmpty {
            self.showAlert("Thông Báo", "Email và password không được để trống", handleOke: nil)
            return
        }
        
        if isValidEmail(email) == false {
            self.showAlert("Thông báo ", "Không đúng định dạng email")
            return
        }
        
        if password.count < 6 {
            self.showAlert("Thông báo", "password không được nhỏ hơn 6 ký tự")
            return
        }
        
        switch type {
        case .login:
            FirebaseManager.loginEmail(email: email, password: password) { (result) -> (Void) in
                switch result {
                    case .sucsses(_):
                        self.showAlert("Thông báo", "Bạn đã đăng nhập thành công")
                    case.error(let err):
                        self.showAlert("Thông báo", "\(err!.localizedDescription)")
                }
            }
            break
        case .register:
            FirebaseManager.registerUserWithEmail(email: email, password: password) { (result) -> (Void) in
                switch result {
                case.sucsses(_):
                    self.showAlert("Thông báo", "Bạn đã đăng ký thành công")
                case.error(let err):
                    self.showAlert("Thông báo", "\(err!.localizedDescription)")
                }
            }
            break
            
        }
    }
    
    //MARK: LOGIN GOOGLE
    func loginGG(){
        FirebaseManager.createUserWithGoogle(view: self)
        
    }
    func handleNotificationCenter(){
        NotificationCenter.default.addObserver(self, selector: #selector(loginSuccess), name: .notificationLoginSuccess, object: nil)
    }
    @objc func loginSuccess(){
        self.showAlert("Thông báo", "Đăng nhập thành công")
    }
    func loginFB(){
        FirebaseManager.createUserWithFacebook(view: self)
    }
    
}
